FROM php:7.1.21-fpm-stretch

ADD ./src /app

WORKDIR /app

CMD php -S 0.0.0.0:80