<!doctype html>
<?php include "head-top.php"; ?>
<!-- Specific Page Vendor CSS -->
<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.carousel.css" />
<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.theme.default.css" />
<?php include "head-bottom.php"; ?>
<body>
    <section class="body">
        <?php include "header.php"; ?>

        <div class="inner-wrapper">
            <?php include "aside.php"; ?>

            <section role="main" class="content-body">
                <?php
                $title = "Credenciamento";
                $subtitle = "Seus credenciamentos";
                $page = "Credenciamento";
                include "title.php";
                $number = 0;
                ?>

                <!-- start: page -->
                <section class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div>
                                    <div class="div-fake-resumo"></div>
                                    <div class="text-md text-right p-xs pr-none" style="border-bottom: 1px solid #fff;">Recebimentos</div>
                                    <div class="text-md text-right p-xs pr-none" style="border-bottom: 1px solid #fff;">Antecipações</div>
                                    <div class="text-md text-right p-xs pr-none" style="border-bottom: 1px solid #fff;">Ajustes e Chargebacks</div>
                                    <div class="text-md text-right p-xs pr-none" style="border-bottom: 1px solid #fff;">Cancelamentos</div>
                                    <div class="text-md text-right p-xs pr-none" style="border-bottom: 1px solid #fff;">Outros</div>
                                    <div class="text-md text-right p-lg pr-none" style="border-bottom: 1px solid #fff;">Saldo</div>
                                </div>
                            </div>
                            <div class="col-md-10 col-md-9-pl-none">
                                <section class="panel panel-resumo">
                                    <div class="panel-body panel-body-resumo">
                                        <div class="owl-carousel owl-theme" data-plugin-carousel="true" data-plugin-options='{ "center": true, "dots": false, "nav": true, "navText":["",""], "items": 5 }'>
                                            <?php for ($i = 1; $i <= 31; $i++) { ?>
                                                <div class="item">
                                                    <div class="text-center text-lg p-sm text-weight-bold"><?php echo $i; ?>/05</div>
                                                    <div class="text-center text-md p-xs odt">R$0,00</div>
                                                    <div class="text-center text-md p-xs">R$0,00</div>
                                                    <div class="text-center text-md p-xs odt">R$0,00</div>
                                                    <div class="text-center text-md p-xs">R$0,00</div>
                                                    <div class="text-center text-md p-xs odt">R$0,00</div>
                                                    <div class="bg-green text-center">
                                                        <sup class="text-center text-weight-bold">Saldo</sup>
                                                        <p class="text-center mb-none text-lg text-secondary text-weight-bold">R$0,00</p>
                                                        <sub class="text-center text-weight-bold">Pago</sub>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end: page -->
            </section>
        </div>
        <?php //include "aside-right.php"; ?>
    </section>
    <?php include "vendor.php"; ?>

    <!-- Specific Page Vendor -->
    <script src="assets/vendor/owl.carousel/owl.carousel.js"></script>

    <?php include "custom-footer.php"; ?>
    <!-- Examples -->
    <script src="assets/javascripts/pages/examples.calendar.js"></script>
</body>
</html>