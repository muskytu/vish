<header class="page-header">
	<h2><?php echo $title; ?></h2>
	<p class="pt-md pl-none col-md-3"><?php echo $subtitle; ?></p>

	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li><a href="#"><i class="fa fa-home"></i></a></li>
			<li><span class="pr-sm"><?php echo $page; ?></span></li>
		</ol>

		<!--<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>-->
	</div>
</header>