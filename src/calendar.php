<!doctype html>
<?php include "head-top.php"; ?>
<!-- Specific Page Vendor CSS -->
<link rel="stylesheet" href="assets/vendor/jquery-ui/jquery-ui.css" />
<link rel="stylesheet" href="assets/vendor/jquery-ui/jquery-ui.theme.css" />
<link rel="stylesheet" href="assets/vendor/fullcalendar/fullcalendar.css" />
<link rel="stylesheet" href="assets/vendor/fullcalendar/fullcalendar.print.css" media="print" />
<?php include "head-bottom.php"; ?>
	<body>
		<section class="body">
			<?php include "header.php"; ?>
			
			<div class="inner-wrapper">
				<?php include "aside.php"; ?>

				<section role="main" class="content-body">
					<?php
						$title = "Calendar";
						$page = "calendar";
						include "title.php";
					?>

					<!-- start: page -->
					<section class="panel">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-9">
									<div id="calendar"></div>
								</div>
								<div class="col-md-3">
									<p class="h4 text-weight-light">Draggable Events</p>

									<hr />

									<div id='external-events'>
										<div class="external-event label label-default" data-event-class="fc-event-default">Default Event</div>
										<div class="external-event label label-primary" data-event-class="fc-event-primary">Primary Event</div>
										<div class="external-event label label-success" data-event-class="fc-event-success">Success Event</div>
										<div class="external-event label label-warning" data-event-class="fc-event-warning">Warning Event</div>
										<div class="external-event label label-info" data-event-class="fc-event-info">Info Event</div>
										<div class="external-event label label-danger" data-event-class="fc-event-danger">Danger Event</div>
										<div class="external-event label label-dark" data-event-class="fc-event-dark">Dark Event</div>

										<hr />
										<div>
											<div class="checkbox-custom checkbox-default ib">
												<input id="RemoveAfterDrop" type="checkbox"/>
												<label for="RemoveAfterDrop">remove after drop</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- end: page -->
				</section>
			</div>
			<?php //include "aside-right.php"; ?>
		</section>
		<?php include "vendor.php"; ?>
		
		<!-- Specific Page Vendor -->
		<script src="assets/vendor/jquery-ui/jquery-ui.js"></script>
		<script src="assets/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js"></script>
		<script src="assets/vendor/moment/moment.js"></script>
		<script src="assets/vendor/fullcalendar/fullcalendar.js"></script>

		<?php include "custom-footer.php"; ?>
		<!-- Examples -->
		<script src="assets/javascripts/pages/examples.calendar.js"></script>
	</body>
</html>