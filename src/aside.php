<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

	<div class="sidebar-header">
		<div class="sidebar-title">
			Menu
		</div>
		<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
			<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>

	<div class="nano">
		<div class="nano-content">
			<nav id="menu" class="nav-main" role="navigation">
			
				<ul class="nav nav-main">
					<li>
						<a href="resumo.php">
							<i class="fa fa-home" aria-hidden="true"></i>
							<span>Resumo</span>
						</a>                        
					</li>
					<li>
						<a href="vendas.php">
							<i class="fa fa-shopping-bag" aria-hidden="true"></i>
							<span>Vendas</span>
						</a>                        
					</li>
					<li>
						<a href="recebimentos.php">
							<i class="fa fa-usd" aria-hidden="true"></i>
							<span>Recebimentos</span>
						</a>                        
					</li>
					<li>
						<a href="busca.php">
							<i class="fa fa-search" aria-hidden="true"></i>
							<span>Busca</span>
						</a>                        
					</li>
				</ul>
			</nav>

			<hr class="separator m-xs" />

			<div class="sidebar-widget widget-tasks">
				<div class="widget-content">
					<ul class="list-unstyled m-none">
						<li><a href="#">Usuário<br />ID 80</a></li>
					</ul>
				</div>
			</div>

			<hr class="separator m-xs" />
			
			<nav id="menu-logout" class="nav-main" role="navigation">
				<ul class="nav nav-main">
					<li>
						<a href="estabelecimentos.php">
							<i class="fa fa-building" aria-hidden="true"></i>
							<span>Estabelecimentos</span>
						</a>                        
					</li>
					<li>
						<a href="credenciamento.php">
							<i class="fa fa-key" aria-hidden="true"></i>
							<span>Credenciamento</span>
						</a>                        
					</li>
					<li>
						<a href="#">
							<i class="fa fa-sign-out" aria-hidden="true"></i>
							<span>Sair</span>
						</a>                        
					</li>
				</ul>
			</nav>
		</div>

		<script>
			// Maintain Scroll Position
			if (typeof localStorage !== 'undefined') {
				if (localStorage.getItem('sidebar-left-position') !== null) {
					var initialPosition = localStorage.getItem('sidebar-left-position'),
						sidebarLeft = document.querySelector('#sidebar-left .nano-content');
					
					sidebarLeft.scrollTop = initialPosition;
				}
			}
		</script>
		
	</div>

</aside>
<!-- end: sidebar -->