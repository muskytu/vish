<!doctype html>
<?php include "head-top.php"; ?>
<!-- Specific Page Vendor CSS -->
<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.carousel.css" />
<link rel="stylesheet" href="assets/vendor/owl.carousel/assets/owl.theme.default.css" />
<?php include "head-bottom.php"; ?>
	<body>
		<section class="body">
			<?php include "header.php"; ?>
			
			<div class="inner-wrapper">
				<?php include "aside.php"; ?>

				<section role="main" class="content-body">
					<?php
						$title = "Calendar";
						$page = "calendar";
						include "title.php";
					?>

					<!-- start: page -->
					<section class="panel">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">
									<div>
										<div>Recebimentos</div>
										<div>Antecipações</div>
										<div>Ajustes e Chargebacks</div>
										<div>Cancelamentos</div>
										<div>Outros</div>
										<div>Saldo</div>
									</div>
								</div>
								<div class="col-md-9">
									<section class="panel">
										<div class="panel-body">
											<div class="owl-carousel owl-theme" data-plugin-carousel data-plugin-options='{ "dots": false, "nav": true, "items": 5 }'>
												<div class="item">
													<div class="text-center">01/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">02/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none bold">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">03/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">04/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">05/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">06/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">07/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">08/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">09/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">10/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">11/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">12/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">13/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">14/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">15/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">16/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">17/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">18/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">19/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">20/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">21/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">22/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">23/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">24/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">25/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">26/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">27/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">28/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">29/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
												<div class="item">
													<div class="text-center">30/04</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="text-center">R$0,00</div>
													<div class="bg-green text-center">
														<sup class="text-center">Saldo</sup>
														<p class="text-center mb-none">R$0,00</p>
														<sub class="text-center">Pago</sub>
													</div>
												</div>
											 </div>
										</div>
									</section>
								</div>
							</div>
						</div>
					</section>
					<!-- end: page -->
				</section>
			</div>
			<?php //include "aside-right.php"; ?>
		</section>
		<?php include "vendor.php"; ?>
		
		<!-- Specific Page Vendor -->
		<script src="assets/vendor/owl.carousel/owl.carousel.js"></script>

		<?php include "custom-footer.php"; ?>
		<!-- Examples -->
		<script src="assets/javascripts/pages/examples.calendar.js"></script>
	</body>
</html>