<!doctype html>
<?php include "head-top.php"; ?>
<!-- Specific Page Vendor CSS -->
<link rel="stylesheet" href="assets/vendor/jquery-ui/jquery-ui.css" />
<link rel="stylesheet" href="assets/vendor/jquery-ui/jquery-ui.theme.css" />
<link rel="stylesheet" href="assets/vendor/select2/css/select2.css" />
<link rel="stylesheet" href="assets/vendor/select2-bootstrap-theme/select2-bootstrap.min.css" />
<link rel="stylesheet" href="assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
<link rel="stylesheet" href="assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css" />
<link rel="stylesheet" href="assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
<link rel="stylesheet" href="assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css" />
<link rel="stylesheet" href="assets/vendor/dropzone/basic.css" />
<link rel="stylesheet" href="assets/vendor/dropzone/dropzone.css" />
<link rel="stylesheet" href="assets/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css" />
<link rel="stylesheet" href="assets/vendor/summernote/summernote.css" />
<link rel="stylesheet" href="assets/vendor/codemirror/lib/codemirror.css" />
<link rel="stylesheet" href="assets/vendor/codemirror/theme/monokai.css" />
<link rel="stylesheet" href="assets/vendor/jquery-datatables-bs3/assets/css/datatables.css" />
<?php include "head-bottom.php"; ?>
<body>
    <section class="body">
        <?php include "header.php"; ?>

        <div class="inner-wrapper">
            <?php include "aside.php"; ?>

            <section role="main" class="content-body">
                <?php
                $title = "Recebimentos";
                $subtitle = "Extrato financeiro";
                $page = "Recebimentos";
                include "title.php";
                $number = 0;
                ?>

                <!-- start: page -->
                <section class="panel">
                    <div class="panel-body">
                        <form>
                            <div class="row mb-md">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-12 control-label">Últimas lançamentos</label>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group btn-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <select class="form-control" data-plugin-multiselect="true" data-plugin-options='{ "maxHeight": 200 }' id="ms_example4">
                                                        <option value="3">3 dias</option>
                                                        <option value="7">7 dias</option>
                                                        <option value="15">15 dias</option>
                                                        <option value="30">30 dias</option>
                                                        <option value="60">60 dias</option>
                                                        <option value="90">90 dias</option>
                                                        <option value="120">120 dias</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-12 control-label">Data por faixa</label>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-daterange input-group" data-plugin-datepicker="true" data-plugin-options='{"language": "pt-BR"}'>
                                                    <span class="input-group-addon" style="border-left: 1px solid #ccc;">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    <input type="text" class="form-control" name="start">
                                                    <span class="input-group-addon">até</span>
                                                    <input type="text" class="form-control" name="end">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12 pt-lg">
                                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary btn-block"><i class="fa fa-search"></i> Buscar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>

                <!-- start: page -->
                <section class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12"><span class="text-tertiary text-weight-bold">Disponível</span></div>
                                </div>
                                <table class="table table-striped table-bordered table-hover mb-none">
                                    <thead>
                                        <tr>
                                            <th>Data</th>
                                            <th>Código</th>
                                            <th>Descrição</th>
                                            <th>Valor (R$)</th>
                                            <th>Saldo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>07/05/2018 12:15</td>
                                            <td>KHJASOE3218912KJS33845</td>
                                            <td>Pagamento de João da Veiga</td>
                                            <td>77,30</td>
                                            <td>77,30</td>
                                        </tr>
                                        <tr>
                                            <td>01/05/2018 12:15</td>
                                            <td>KHJASOE3218912KJS33845</td>
                                            <td>Ivo Pintangui</td>
                                            <td>77,30</td>
                                            <td>154,60</td>
                                        </tr>
                                        <tr>
                                            <td>08/05/2018 12:15</td>
                                            <td>KHJASOE3218912KJS33845</td>
                                            <td>Pagamento de Joaquim Salomão</td>
                                            <td>77,30</td>
                                            <td>231,90</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr class="short" />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12"><span class="text-info text-weight-bold">A receber</span></div>
                                </div>
                                <table class="table table-striped table-bordered table-hover mb-none">
                                    <thead>
                                        <tr>
                                            <th>Data</th>
                                            <th>Código</th>
                                            <th>Descrição</th>
                                            <th>Valor (R$)</th>
                                            <th>Saldo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>07/05/2018 12:15</td>
                                            <td>KHJASOE3218912KJS33845</td>
                                            <td>Pagamento de João da Veiga</td>
                                            <td>77,30</td>
                                            <td>77,30</td>
                                        </tr>
                                        <tr>
                                            <td>01/05/2018 12:15</td>
                                            <td>KHJASOE3218912KJS33845</td>
                                            <td>Ivo Pintangui</td>
                                            <td>77,30</td>
                                            <td>154,60</td>
                                        </tr>
                                        <tr>
                                            <td>08/05/2018 12:15</td>
                                            <td>KHJASOE3218912KJS33845</td>
                                            <td>Pagamento de Joaquim Salomão</td>
                                            <td>77,30</td>
                                            <td>231,90</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr class="short" />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12"><span class="text-danger text-weight-bold">Bloqueado</span></div>
                                </div>
                                <table class="table table-striped table-bordered table-hover mb-none">
                                    <thead>
                                        <tr>
                                            <th>Data</th>
                                            <th>Código</th>
                                            <th>Descrição</th>
                                            <th>Valor (R$)</th>
                                            <th>Saldo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>07/05/2018 12:15</td>
                                            <td>KHJASOE3218912KJS33845</td>
                                            <td>Pagamento de João da Veiga</td>
                                            <td>77,30</td>
                                            <td>77,30</td>
                                        </tr>
                                        <tr>
                                            <td>01/05/2018 12:15</td>
                                            <td>KHJASOE3218912KJS33845</td>
                                            <td>Ivo Pintangui</td>
                                            <td>77,30</td>
                                            <td>154,60</td>
                                        </tr>
                                        <tr>
                                            <td>08/05/2018 12:15</td>
                                            <td>KHJASOE3218912KJS33845</td>
                                            <td>Pagamento de Joaquim Salomão</td>
                                            <td>77,30</td>
                                            <td>231,90</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end: page -->
            </section>
        </div>
        <?php //include "aside-right.php"; ?>
    </section>
    <?php include "vendor.php"; ?>

    <!-- Specific Page Vendor -->
    <script src="assets/vendor/jquery-ui/jquery-ui.js"></script>
    <script src="assets/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js"></script>
    <script src="assets/vendor/select2/js/select2.js"></script>
    <script src="assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
    <script src="assets/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
    <script src="assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
    <script src="assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script src="assets/vendor/bootstrap-timepicker/bootstrap-timepicker.js"></script>
    <script src="assets/vendor/fuelux/js/spinner.js"></script>
    <script src="assets/vendor/dropzone/dropzone.js"></script>
    <script src="assets/vendor/bootstrap-markdown/js/markdown.js"></script>
    <script src="assets/vendor/bootstrap-markdown/js/to-markdown.js"></script>
    <script src="assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js"></script>
    <script src="assets/vendor/codemirror/lib/codemirror.js"></script>
    <script src="assets/vendor/codemirror/addon/selection/active-line.js"></script>
    <script src="assets/vendor/codemirror/addon/edit/matchbrackets.js"></script>
    <script src="assets/vendor/codemirror/mode/javascript/javascript.js"></script>
    <script src="assets/vendor/codemirror/mode/xml/xml.js"></script>
    <script src="assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="assets/vendor/codemirror/mode/css/css.js"></script>
    <script src="assets/vendor/summernote/summernote.js"></script>
    <script src="assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js"></script>
    <script src="assets/vendor/ios7-switch/ios7-switch.js"></script>
    <script src="assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js"></script>
    <script src="assets/vendor/select2/js/select2.js"></script>
    <script src="assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
    <script src="assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
    <script src="assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

    <?php include "custom-footer.php"; ?>
    <!-- Examples -->
    <script src="assets/javascripts/forms/examples.advanced.form.js"></script>
    <script src="assets/javascripts/tables/examples.datatables.default.js"></script>
    <script src="assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
    <script src="assets/javascripts/tables/examples.datatables.tabletools.js"></script>
    <script>
        $("#selectAll").change(function () {
            if (this.checked) {
                $("#textSelect").text("Desmarcar tudo");
                $("input:checkbox").each(function () {
                    this.checked = true;
                });
            } else {
                $("#textSelect").text("Marcar tudo");
                $("input:checkbox").each(function () {
                    this.checked = false;
                });
            }
        });
    </script>
</body>
</html>